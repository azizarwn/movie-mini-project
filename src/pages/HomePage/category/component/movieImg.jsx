import React, { useEffect } from 'react'
import { GetAllMovie } from '../../../../store/action/allMovie'
import { useSelector, useDispatch } from 'react-redux'
import Loading from './loading'
import { Row, Col } from 'react-bootstrap'
import PaginationHomepage from '../../pagination/pagination'


const MovieImg = ({...props}) => {
   
    const dispatch = useDispatch()
    const allMovie = useSelector(state => state.AllMovie.allMovies)
    const filterGenre = useSelector(state =>state.AllMovie.filterGenre)
    const loadGenre = useSelector(state => state.AllMovie.loading)
    
    useEffect(() => {
        dispatch(GetAllMovie())
	}, [])

    
    return (
       
        <div className='img-category'>
            <Row className="home-row" style={{display: 'flex', justifyContent: 'center', textAlign: 'center'}}>
                    { loadGenre ? 
                        <Loading /> : props.all ? allMovie?.map((item, index) => {
                            return(
                                    <Col key={index} className="home-col"  >
                                        <a href={`/DetailPage/${item.id}`} >
                                            <img className="img-movie" src={item.poster} alt={item.title}/>
                                            <div className="img-text">
                                                <p className="title">{item.title}</p>
                                            </div>
                                        </a>
                                    </Col>
                            )
                        }) :
                        filterGenre?.map((item, index) => {
                            return(
                                    <Col key={index} className="home-col"  >
                                         <a href={`/DetailPage/${item.id}`} >
                                            <img className="img-movie" src={item.poster} alt={item.title}/>
                                            <div className="img-text">
                                                <p className="title">{item.title}</p>
                                            </div>
                                        </a>
                                    </Col>
                            );
                        })}
             </Row>
            
                     
        </div>
        
    )
}

export default MovieImg
