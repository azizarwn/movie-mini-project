import {
    GET_DATA_REVIEW_BEGIN,
    GET_DATA_REVIEW_SUCCESS,
    GET_DATA_REVIEW_FAIL
} from "./actionTypes"

import axios from 'axios'

export const GetReviewData = (id) => (dispatch) => {
    dispatch({
        type: GET_DATA_REVIEW_BEGIN,
        loading: true,
        error: null,
    })

    axios
        .get(`/movie/getReview/${id}`,
            {headers: { 'Content-Type': 'application/json' }}
        )
        .then(res => {
            console.log(res)
            dispatch({ 
                type: GET_DATA_REVIEW_SUCCESS,
                loading: false, 
                payload: res.data.data.docs,
            })
        })

        .catch((err) =>
            dispatch({
                type: GET_DATA_REVIEW_FAIL,
                loading: false,
                error: err,
            })
        )

}