import {
    GET_DATA_REVIEW_BEGIN,
    GET_DATA_REVIEW_SUCCESS,
    GET_DATA_REVIEW_FAIL
} from "../action/actionTypes"

const initialState = {
    loading: false,
    error: null,
    reviews: [],
}

const ReviewData = (state = initialState, action) => {
    const {type, payload, error} = action;
    switch (type) {
        default: 
            return {
                ...state,
            };
        case GET_DATA_REVIEW_BEGIN:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case GET_DATA_REVIEW_SUCCESS:
            return {
                ...state,
                loading: false,
                reviews: payload,
                error: null,
            }
        case GET_DATA_REVIEW_FAIL:
            return {
                ...state,
                loading: false,
                error: error,
                reviews: []
            }
    }
};

export default ReviewData;