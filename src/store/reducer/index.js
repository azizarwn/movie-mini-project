import {combineReducers} from "redux"
import ReviewData from './reviewData'
import { AllMovie, ByGenre, PaginPage } from './allMovie'
import { GetDetailPage } from './detailMovie'


const rootReducers = combineReducers({
    ReviewData,
    AllMovie,
    ByGenre,
    GetDetailPage,
    PaginPage,
})

export default rootReducers