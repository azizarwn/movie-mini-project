import React, { useState } from 'react'
import { FaStar } from 'react-icons/fa'
import './starRating.scss'

function StarRating({...props}) {
    const [rating, setRating] = useState(0);
    const [hover, setHover] = useState(null);
    console.log(props)
    return (
        <div className="rating-container">
            {[...Array(5)].map((star, index) => {
                const ratingValue = index + 1;
                return (
                    <label key={index}>
                        <input type="radio" 
                        name="rating" 
                        value={props.rates} 
                        onClick = {() => setRating(ratingValue)}
                    />
                        <FaStar
                            className="star" 
                            color={ratingValue <= (hover || rating) ? "#ffc107" : "#333333"} 
                            size={30}
                            onMouseEnter = {() => setHover(ratingValue)}
                            onMouseLeave = {() => setHover(null)} 
                        />
                    </label>
                )
            })}
            <p>Rating: {rating}/5</p>
        </div>
    )
}

export default StarRating
