import React, { useState, useEffect } from "react";
import {
  Form,
  Navbar,
  Nav,
  FormControl,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Badge,
} from "react-bootstrap";
import "./modal.css";
import logo from "./logo2.png";
import pict from "./kaka2.png";
// import SearchBox from "./SearchBox";

const Header = (props) => {
  // const { buttonLabel, className } = props;

  // const [modal, setModal] = useState(false);

  // const toggle = () => setModal(!modal);
  const [movies, setMovies] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [change, setChange] = useState("");
  const [register, setRegister] = useState(false);

  // const getMovieRequest = async (searchValue) => {
  //   const url = `/movie/search?page=1&limit=10&genre=Action&title=${searchValue}&release_date=2009,2021`;
    
  //   fetch (url,
  //     {headers: { 
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json'
  //      }} 
  //   )
  //   .then (res => res.json())
  //   .then (data => console.log (data))
  // };

  
  const toggle = (e) => {
    e.preventDefault();
    setRegister(!register);
  };
  
  console.log(register);
  console.log(change)

  // useEffect(() => {
  // getMovieRequest(change);
  // }, [change]);

  return (
    <>
      <style type="text/css">
        {`
      .btn-flat {
        background-color: #fe024e;
        color: white;
        letter-spacing: 0.00px;
        margin-right: 50px;
        margin-top: 6.0px;
        min-height: 56px;
        min-width: 149px;
        border-radius: 40px;
      }
  
      .btn-xxl {
        padding: 1rem 1.5rem;
        font-size: 1.5rem;
      }
      `}
      </style>
      <Navbar className="background" bg="light" variant="light">
        <Navbar.Brand href="#home"></Navbar.Brand>
        <Nav className="mr-auto navbar-title">
          <a className="navbar-title" href="/">
          <img className="logo" src={logo} alt="logo"/>
          <h4 style={{ color: "#fe024e", fontWeight: "700" }}>
            Watch Movies Online
          </h4>
          </a>
          <Form inline>
          {/* <SearchBox
            // onChange={(event) => setChange(event.target.value)}
            style={{paddingLeft: '150px'}}
            setChange={setChange}
            searchValue={searchValue}
            setSearchValue={setSearchValue}
          /> */}
          </Form>
        </Nav>
        <Form inline>
        
          {/* <FormControl
            type="text"
            placeholder="Search Movie"
            className="mr-sm-2 overlap-group1"
            style={{ width: "35rem" }}
          /> */}
     
          {/* Sign In Button  */}
          {/* <Button variant="flat" size="xxl">Sign In</Button> */}
          <Button
            type="button"
            variant="flat"
            className="btn btn-primary"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            Sign Up
          </Button>         
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <img
                    className="logo"
                    src={logo}
                    alt="logo"
                    style={{ paddingLeft: "180px" }}
                  />
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>

                {/* Sign Up */}
                {register ? (
                  <>
                    <div className="modal-body">
                      <Form className="modal-form">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                          className="modal-input"
                          type="email"
                          placeholder="msgranger@mail.com"
                        />
                        <br />
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          className="modal-input"
                          type="password"
                          placeholder="Password"
                        />
                      </Form>
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-primary modal-btn"
                      >
                        Sign In
                      </button>
                      <div className="modal-login">
                        <p
                          href="#"
                          style={{ paddingTop: "24px", paddingRight: "24px" }}
                        >
                          Don't have an account?
                        </p>
                        <Button
                          onClick={toggle}
                          style={{
                            borderRadius: "50px",
                            backgroundColor: "#fe024e",
                          }}
                        >
                          {" "}
                          Sign Up
                        </Button>
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    {/* Login */}
                    <div className="modal-body">
                      <Form className="modal-form">
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control
                          className="modal-input"
                          type="type"
                          placeholder="Hermione Granger"
                        />
                        <br />
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                          className="modal-input"
                          type="email"
                          placeholder="msgranger@mail.com"
                        />
                        <br />
                        <Form.Label> Password</Form.Label>
                        <Form.Control
                          className="modal-input"
                          type="password"
                          placeholder="Password"
                        />
                      </Form>
                    </div>
                    <div className="modal-footer">
                      <button
                        onClick={toggle}
                        type="button"
                        className="btn btn-primary modal-btn"
                      >
                        Sign Up
                      </button>
                      <div className="modal-login">
                        <p style={{ paddingTop: "24px", paddingRight: "24px" }}>
                          Already have an account?{" "}
                        </p>
                        <Button
                          onClick={toggle}
                          style={{
                            borderRadius: "50px",
                            backgroundColor: "#fe024e",
                          }}
                        >
                          {" "}
                          Sign in
                        </Button>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </Form>
      </Navbar>
    </>
  );
};

export default Header;
