import React, { useState, useEffect } from "react";
import {useDispatch, useSelector} from 'react-redux'
import imgChara from './assets/img-chara.jpg'
import './character.css'
import { Card, Row, Col } from 'react-bootstrap'
import {GetDetailMovie} from '../../../store/action/getDetailMovie'

function Character() {
  const  dispatch = useDispatch();
  const detail = useSelector((state) => state.GetDetailPage.getDetail);

  const id = window.location.pathname.slice(-24)
  useEffect(() => {dispatch(GetDetailMovie(id))}
  , [ ]) 

    return (
        
        <div className='img-character'>
            {detail.map((item, index) => {
              return(
                <div className="char-col">
                  <Row className="home-row" sm={1} md={2} lg={6} >
                  {item.characters.map((char, index) => {
                    return(
                      <Col key={index} className="home-col" >
                        <Card className="cast-card" style={{ width: "9rem", backgroundColor: "#EB507F", display : "inline-flex" }}>
                          <Card.Img variant="top" src={char.photo} />
                          <Card.Body>
                            <Card.Subtitle >{char.role_name}</Card.Subtitle >
                          </Card.Body>
                        </Card>
                      </Col>  
                    )
                  })}
                  </Row> 
                </div>
              )})}                  
        </div>
    )
}

export default Character;
