import React, { useEffect } from 'react'
import { GetReviewData } from '../../../../store/action/reviewData'
import { useSelector, useDispatch } from 'react-redux'
//import StarRating from './starRating';
import Rating from './Rating';


const ReviewDataUser = () => {
    const dispatch = useDispatch();
    const dataReview = useSelector((state) => state.ReviewData.reviews)
    const loading = useSelector((load) => load.ReviewData.loading)

    console.log('res',dataReview)
    console.log('laod',loading)

    const id = window.location.pathname.slice(-24)
    useEffect(() => {
        dispatch(GetReviewData(id));
    }, []);

    return (
        <div>
            {loading?
                "loading..." : dataReview &&
                dataReview.map((user, index) => {
                    return (
                        <div className="review-user" key={index}>
                            <img className="user-pic" src={user.user_id.profile_picture} alt={user.user_id.name}/>
                            <div className="user-item">
                                <h4>{user.user_id.name}</h4>
                                <Rating rates={user.rating} />
                                {/* <StarRating rates={user.rating}/> */}
                                <p>{user.review}</p>
                            </div>
                    </div> 
                    )
                })
            }
        </div>
    )
}

export default ReviewDataUser;