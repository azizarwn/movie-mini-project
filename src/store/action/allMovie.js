import {
    GET_ALL_MOVIE_BEGIN,
    GET_ALL_MOVIE_SUCCESS,
    GET_ALL_MOVIE_FAIL,

    GET_BY_GENRE_BEGIN,
    GET_BY_GENRE_SUCCESS,
    GET_BY_GENRE_FAIL,

    GET_MOVIE_BY_GENRE_BEGIN,
    GET_MOVIE_BY_GENRE_SUCCESS,
    GET_MOVIE_BY_GENRE_FAIL,

    GET_PAGE_BEGIN,
    GET_PAGE_SUCCESS,
    GET_PAGE_FAIL,
} from "./actionTypes"

import axios from 'axios'

export const GetAllMovie = (page=1) => (dispatch) => {
    
    dispatch({
        type: GET_ALL_MOVIE_BEGIN,
        loading: true,
        error: null,
    })

    axios
    .get(`/movie/getAll?page=${page}`,
        {headers: { 'Content-Type': 'application/json' }}
    )
    .then(res => {
        console.log(res)
        dispatch({ 
            type: GET_ALL_MOVIE_SUCCESS, 
            loading: false,
            payload: res.data.data,
            genre: 'all',
            activePage: page,
        })
    })
    .catch((err) =>
        dispatch({
            type: GET_ALL_MOVIE_FAIL,
            loading: false,
            error: err,
        })
    )
}

export const GetByGenre = () => (dispatch) => {
    dispatch({
        type: GET_BY_GENRE_BEGIN,
        loading: true,
        error: null,
    })

    axios
    .get(`/genre/getMain`,
        {headers: { 'Content-Type': 'application/json' }}
    )
    .then(res => {
        console.log(res)
        dispatch({ 
            type: GET_BY_GENRE_SUCCESS, 
            loading: false,
            payload: res.data.data, 
        })
    })
    .catch((err) =>
        dispatch({
            type: GET_BY_GENRE_FAIL,
            loading: false,
            error: err,
        })
    )
}

export const GetMovieByGenre = (genre, page=1) => (dispatch) => {
    dispatch({
        type: GET_MOVIE_BY_GENRE_BEGIN,
        loading: true,
        error: null,
    })

    axios
    .get(`/movie/search?genre=${genre}&page=${page}`,
        {headers: { 'Content-Type': 'application/json' }}
    )
    .then(res => {
        console.log(res)
        dispatch({ 
            type: GET_MOVIE_BY_GENRE_SUCCESS,
            loading: false,
            payload: res.data.data,
            genre: genre,
            activePage: page,
        })
    })
    .catch((err) =>
        dispatch({
            type: GET_MOVIE_BY_GENRE_FAIL,
            loading: false,
            error: err,
        })
    )
}

export const GetPagin = (genre, page = 1) => (dispatch) => {
    dispatch({
        type: GET_PAGE_BEGIN,
        loading: true,
        error: null,
    })
    let actionUrl = 'getAll'
    if (genre !== 'All') {
        actionUrl = 'search?genre='+genre; 
    }
    axios
    .get(`/movie/getAll?`,
        {headers: { 'Content-Type': 'application/json' }}
    )
    .then(res => {
        console.log(res)
        dispatch({ 
            type: GET_PAGE_SUCCESS,
            loading: false,
            pagin: res.data.data, 
        })
    })
    .catch((err) =>
        dispatch({
            type: GET_PAGE_FAIL,
            loading: false,
            error: err,
        })
    )
}