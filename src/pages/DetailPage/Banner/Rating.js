import React from 'react'
// import './starRating.scss'
import StarRatingComponent from 'react-star-rating-component'
import { FaStar } from 'react-icons/fa';


function Rating({...props}) {
    return (
        <div>
            <StarRatingComponent 
                value={props.rates}
                name="rate2" 
                editing={false}
                starCount={5}
                renderStarIcon={() => 
                    <span>
                        <FaStar 
                        size={30} 
                        />
                    </span>}
            />
        </div>
    )
}

export default Rating
