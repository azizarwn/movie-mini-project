import React from 'react'
// import { InputGroup } from 'react-bootstrap';
import "./modal.css"

const SearchBox = (props) => {
    return (
        <div>
            <input
            type="text"
            placeholder="Search Movie"
            className="search-item mr-sm-2 overlap-group1"
            style={{ width: "34rem", marginLeft: "100px"}}
            value={props.value}
            onChange={(event) => props.setChange(event.target.value)}
            placeholder="Type of Search..."
            ></input>
        </div>
    );
};

export default SearchBox
