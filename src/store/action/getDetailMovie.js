import {
    GET_DETAIL_MOVIE_BEGIN,
    GET_DETAIL_MOVIE_SUCCESS,
    GET_DETAIL_MOVIE_FAIL,
} from "./actionTypes"


import axios from 'axios'


export const GetDetailMovie = (id) => (dispatch) => {
    dispatch({
        type: GET_DETAIL_MOVIE_BEGIN,
        loading: true,
        error: null,
    })

    axios
        .get(`/movie/detail/${id}`,
            {headers: { 'Content-Type': 'application/json' }}
        )
        .then(res => {
            console.log("response",res)
            dispatch({ 
                type: GET_DETAIL_MOVIE_SUCCESS,
                loading: false, 
                payload: res.data.data,
            })
        })

        .catch((err) =>
            dispatch({
                type: GET_DETAIL_MOVIE_FAIL,
                loading: false,
                error: err,
            })
        )

}