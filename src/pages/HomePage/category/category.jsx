import React, { useState } from 'react'
import './category.css'
import { Provider } from 'react-redux'
import store from '../../../store'
import ButtonGenre from './component/genreButtons'
import MovieImg from './component/movieImg'
import PaginationHomepage from '../pagination/pagination'

function Category() {
    const [ all, setAll ] = useState(true)
    const [ pagin, setPagin ] = useState(true)

    return (
      <>
      <Provider store={store}>
      <div className="category-container">
        <div className="category-select">
          <h1 className="title-category">Browse by genre</h1>
            <ButtonGenre setAll={setAll} /> 
        </div>

        <div className='img-category'>
            <MovieImg all={all} />
        </div>

        <PaginationHomepage  />

      </div>
      </Provider>
      </>
    )
}

export default Category;
