import React, { useEffect } from 'react'
import { GetByGenre, GetMovieByGenre, GetAllMovie, GetPagin  } from '../../../../store/action/allMovie'
import { useSelector, useDispatch } from 'react-redux'
import { Nav, Row, Col } from 'react-bootstrap'
import './genreButton.css'

const ButtonGenre = ({...props}) => {
    const dispatch = useDispatch()
    const buttonGenre = useSelector(state => state.ByGenre.byGenre)
    const loading = useSelector(state => state.ByGenre.loading)
       

    useEffect(() => {
        dispatch(GetAllMovie())
        dispatch(GetByGenre())
        dispatch(GetMovieByGenre())
        dispatch(GetPagin())
    }, [])

   const AllMovie = () => {
    dispatch(GetAllMovie())
    props.setAll(true)
   }

   const FilterMovie = (item) => {
    dispatch(GetMovieByGenre(item.genre))
    props.setAll(false)
   }

    return (
        <div >
        <Nav className="item-category" variant="pills" defaultActiveKey="all">
            <Nav.Item>
            <Row>
            <Nav.Link 
                eventKey="all"
                onClick={()=>AllMovie()}
                >All</Nav.Link>
                { loading ? 
                    '' : 
                    buttonGenre?.map((item, index) => {
                        return(
                            <>
                            <Col sm={true}>
                                <Nav.Link 
                                    key={index} 
                                    eventKey={item.genre}
                                    onClick={()=>FilterMovie(item)}
                                >
                                    {item.genre}
                                </Nav.Link>
                            </Col>
                          </>
                        );
                    })}
            </Row>
            </Nav.Item>
        </Nav>
        
        </div>
    )
}

export default ButtonGenre;
