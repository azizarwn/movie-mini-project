import React, { useState, useEffect } from "react";
import {useDispatch, useSelector} from 'react-redux'
import "./styles.css";
// import "../Navbar/modal.css";
import Iframe from "react-iframe";
import {GetDetailMovie} from '../../../store/action/getDetailMovie'
import Rating from "./Rating";

function Banner({...props}) {
  const [open, setOpen] = useState("");
  const  dispatch = useDispatch();
    console.log(open);

  const detail = useSelector((state) => state.GetDetailPage.getDetail);
  console.log("detail", detail);

  const id = window.location.pathname.slice(-24)
  useEffect(() => {dispatch(GetDetailMovie(id))}
  , [ ]) 

  return (
    <div>
      {detail.map((item, i) => {return (
        <div className="masthead"
          key = {i}
          style={{
            background: `linear-gradient(to bottom, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0.7) 75%, #000000 100%), url(${item.backdrop})`,
            backgroundSize: "cover",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundAttachment: "fixed"
          }}
        >
        <div className="container d-flex h-100 align-items-center">
          <div className="mx-auto text-left">
            <h1>{item.title}</h1>
            <div className="review">
              <Rating rates={item.avg_rating} />
              
              <div className="address roboto-medium-wild-sand-36px">
              {item.count_review} Reviews
              </div>
            </div>
            <h3>
              {item.synopsis}
            </h3>
            <button
              className="btn btn-danger"
              data-target="#iframe"
              data-toggle="modal"
            >
              Watch Trailer
            </button>
            <div
              className="modal fade"
              id="iframe"
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <Iframe
                    width="560"
                    height="315"
                    position="relative"
                    display="initial"
                    url={`http://www.youtube.com/embed/${item.trailer}`}
                    title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></Iframe>
                </div>
              </div>
            </div>
            
            <button
              type="button"
              className="btn btn-outline-danger"
              data-toggle="modal"
              data-target="#"
              style={{ border: "3px solid white", marginLeft: "24px" }}
            >
              Add Watchlist
            </button>
          </div>
        </div>
      </div>
    )})}
    </div>
  
  );
}
export default Banner;
