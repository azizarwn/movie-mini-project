import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import HomePage from '../pages/HomePage/HomePage'
import DetailPage from "../pages/DetailPage/DetailPage"

const Routes = () => {
    return (
      <Router>
        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/DetailPage/:id" exact>
            <DetailPage/>
          </Route>
          <Route path="/*">
            <h1>Page Not Found</h1>
          </Route>
        </Switch>
      </Router>
    );
  };
  
  export default Routes;