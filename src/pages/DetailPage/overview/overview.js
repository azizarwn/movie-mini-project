import React, { useState, useEffect } from "react"
import { Nav } from 'react-bootstrap'
import './overview.css'
import {GetDetailMovie} from '../../../store/action/getDetailMovie'
import {useDispatch, useSelector} from 'react-redux'



function Overview() {
    const  dispatch = useDispatch();
    const detail = useSelector((state) => state.GetDetailPage.getDetail);

    const id = window.location.pathname.slice(-24)
    useEffect(() => {dispatch(GetDetailMovie(id))}
    , [ ]) 

    const formatBudget = (budget) => {
        return budget.toLocaleString("en-US", {
          style: "currency",
          currency: "USD",
        });
    };

    const rating = (x) => {
        return Number.parseFloat(x).toFixed(2);
    } 

    return (
        <div className="overview-container">
            <div className="synopsis-container">
             {
                detail.map((item, index) => {
                    return(
                    <div key={index} className="home-col" sm={true} md={8} lg={true}>
                        <h4>Synopsis</h4>
                        <hr/>
                        <p>{item.synopsis}</p>
                        <br/>
                        <h4>Movie-info</h4>
                        <hr/>
                        <p>Director : {item.director}</p>
                        <p>Budget : {formatBudget(item.budget)}</p>
                        <p>Rating : {rating(item.avg_rating)}</p>
                        <p>Total Review : {item.count_review} Reviews</p>
                    
                    </div>
                    );
                })
            }                             
            </div> 
            
            {/* <div > 
                
                <hr/>
                    <div className="text-1">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna 
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    </div>

                    <div className="text-1">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna 
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
            </div>

            <div className="Movie-container">
                    
                        <hr/>
                <div className="movie-detail">
                    Release date    : January 5, 1998 <br />
                    Director  : John Doe <br />
                    Featured song  :  Pegasus fantasi <br />
                    Budget   : 200 million USD <br />
                    Release date    : January 5, 1998 <br />
                    Director  : James Cameron <br />
                    Featured song  : Soldier dream <br />
                    Budget   : 200 million USD <br />
                </div>

            </div> */}
            
        
        </div>
    )
}

 
export default Overview
