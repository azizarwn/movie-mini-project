import React from 'react'
import {Row, Col} from 'react-bootstrap'
import './footer.css'
import logo from './assets/logo2.png'
import AppStore from './assets/app-store.png'
import Gplay from './assets/g-play.png'
import FbIcon from './assets/fbIcon.png'
import PinIcon from './assets/pinIcon.png'
import IgIcon from './assets/igIcon.png'

function FooterComponent() {
    return (
        <div className="footer-container">
            <div className="footer-item">
                <div className="footer-main">
                    <Row className="row-description">
                        <Col  sm={6} className="col-description">
                            <div className="brand-logo">
                                <img className="logo-footer" src={logo} alt='logo' />
                                <h5>Watch Movie Online</h5>
                            </div>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                            Labore, nemo facilis accusantium harum vel similique blanditiis
                            provident ipsum nostrum nam voluptatum quasi consectetur praesentium
                            libero quibusdam, sint repellat eius ea?
                            </p>
                        </Col>
                        <Col  sm={2} className="col-about">
                            <p>Tentang Kami</p>
                            <p>Blog</p>
                            <p>Layanan</p>
                            <p>Karir</p>
                            <p>Pusat Media</p>
                        </Col>
                        <Col sm={4} className="col-icon">
                            <h6>Download</h6> 
                            <div className="media-icon">
                                <img className="icon-gplay icon" src={Gplay} alt='icon-gplay'/>
                                <img className="icon-app icon" src={AppStore} alt='icon-appstore'/>
                            </div>
                            <br/>
                            <h6>Social Media</h6>
                            <div className="media-icon">
                                <img className="icon-fb icon" src={FbIcon} alt='icon-FbIcon'/>
                                <img className="icon-pin icon" src={PinIcon} alt='icon-IGIcon'/>
                                <img className="icon-ig icon" src={IgIcon} alt='icon-PinIcon'/>
                            </div> 
                        </Col>
                    </Row>
                </div>
                <hr style={{backgroundColor: 'white'}}/>
                <div className="footer-bottom">
                    <h6>Copyright &copy; 2021 teamaTV. All Rights Reserved</h6>
                </div>
            </div>
        </div>
      );
}

export default FooterComponent
